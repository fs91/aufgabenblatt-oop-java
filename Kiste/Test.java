package Kiste;

public class Test {

	public static void main(String[] args) {
		
		float width = 3;
		float height = 2;
		float depth = 4;
		String color = "blau";
		
		Kiste k = new Kiste(width, height, depth, color);
		
		System.out.println("Die Kiste ist:\n"
				+ " " + width + " Einheiten hoch\n"
				+ " " + height + " Einheiten breit\n"
				+ " " + depth + " Einheiten tief");
		System.out.println("Volumen der Kiste: " + k.getVolumen());
		
	}

}
