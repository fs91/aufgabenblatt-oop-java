package Kiste;

public class Kiste {
	
	private float height;
	private float width;
	private float depth;
	private String color;
	
	Kiste(float height, float width, float depth, String color) {
		this.height = height;
		this.width = width;
		this.depth = depth;
		this.color = color;
	}
	
	public float getVolumen() {		
		return (height * width * depth);
	}
	
	public String getFarbe() {
		return color;
	}
	
}
