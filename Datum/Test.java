package Datum;

public class Test {

	public static void main(String[] args) {
		Datum date = new Datum(15, 12, 2017);
		System.out.println("neues Datum: " + date.getDatum());
		
		date.moveByOneMonth();
		System.out.println("ein Monat spaeter: " + date.getDatum());
		
		date.moveByDays(22);
		System.out.println("22 Tage spaeter: " + date.getDatum());
		
		date.setJahr(2020);
		System.out.println("neues Jahr: " + date.getDatum());
		
		date.setMonat(6);
		System.out.println("neuer Monat: " + date.getDatum());
		
		date.setTag(15);
		System.out.println("neuer Tag: " + date.getDatum());
	}

}
