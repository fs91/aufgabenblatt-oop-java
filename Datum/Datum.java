package Datum;

class Datum {
	
	private int tag;
	private int monat;
	private int jahr;
	
	// Konstruktor mit Abbruchbedingung
	Datum(int tag, int monat, int jahr) {
		if (checkDatum(tag, monat, jahr)) {
			this.tag = tag;
			this.monat = monat;
			this.jahr = jahr;
		} else {
			throw new IllegalArgumentException("\n\t- Das Datum ist ung�ltig."
					+ "\n\t- Bitte aendern Sie das Datum.");
		}
	}
	
	// gibt Datum als String zurueck
	public String getDatum() {
		return "" + tag + "." + monat + "." + jahr;
	}
	
	// prueft und setzt den Tag
	public void setTag(int tag) {
		if (checkDatum(tag, this.monat, this.jahr)) {
			this.tag = tag;
		} else {
			throw new IllegalArgumentException("\n\t- Der Tag ist ung�ltig."
					+ "\n\t- Bitte aendern Sie den Tag.");
		}
		
	}
	
	// prueft und setzt den Monat
	public void setMonat(int monat) {
		if (checkDatum(this.tag, monat, this.jahr)) {
			this.monat = monat;
		} else {
			throw new IllegalArgumentException("\n\t- Der Monat ist ung�ltig."
					+ "\n\t- Bitte aendern Sie den Monat.");
		}
	}
	
	// prueft und setzt das Jahr
	public void setJahr(int jahr) {
		if (checkDatum(this.tag, this.monat, jahr)) {
			this.jahr = jahr;
		} else {
			throw new IllegalArgumentException("\n\t- Das Jahr ist ung�ltig."
					+ "\n\t- Bitte aendern Sie das Jahr.");
		}
	}
	
	// prueft ein einfaches Datum
	public boolean checkDatum() {
		boolean c = false;
		if (this.tag < 32 || this.monat < 13) {
			c = true;
		}
		switch(this.monat) {
		case 2:
			if (this.checkSchaltjahr()) {
				if (this.tag < 30) {
					c = true;
				}
			} else {
				if (this.tag < 29) {
					c = true;
				}
			}
		case 4:
		case 6:
		case 9:
		case 11:
			if (this.tag < 31) {
				c = true;
			}
			break;
		default:
			if (this.tag < 32) {
				c = true;
			}
		}
		return c;
	}
	
	// prueft Datum mit Parametern
	private boolean checkDatum(int tag, int monat, int jahr) {
		boolean c = false;
		if (tag < 32 && monat < 13) {
			c = true;
		}
		switch(monat) {
		case 2:
			if (checkSchaltjahr(jahr)) {
				if (tag < 30) {
					c = true;
				}
			} else {
				if (tag < 29) {
					c = true;
				}
			}
		case 4:
		case 6:
		case 9:
		case 11:
			if (tag < 31) {
				c = true;
			}
			break;
		default:
			if (tag < 32) {
				c = true;
			}
		}
		return c;
	}
	
	// erhoeht den Monat um 1
	public Datum moveByOneMonth() {
		int newM = this.monat + 1;
		if (newM == 13) {
			newM = 1;
			setJahr(this.jahr + 1);
		}
		setMonat(newM);
		return this;
	}
	
	// aendert die Tage des Datums und passt den Monat entsprechend an
	public Datum moveByDays(int days) {
		switch(this.monat) {
			case 2:
				if (this.checkSchaltjahr()) {
					this.tag += days;
					if (this.tag > 29) {
						this.tag -= 29;
						this.moveByOneMonth();
					}
				} else {
					this.tag += days;
					if (this.tag > 28) {
						this.tag -= 28;
						this.moveByOneMonth();
					}
				}
			case 4:
			case 6:
			case 9:
			case 11:
				this.tag += days;
				if (this.tag > 30) {
					this.tag -= 30;
					this.moveByOneMonth();
				}
				break;
			default:
				this.tag += days;
				if (this.tag > 31) {
					this.tag -= 31;
					this.moveByOneMonth();
				}
		}
		return this;
	}
	
	// prueft Schaltjahr eines Datums
	private boolean checkSchaltjahr() {
		if (this.jahr % 400 == 0) {
            return true;
        } else if (this.jahr % 100 == 0) {
            return false;
        } else if (this.jahr % 4 == 0) {
            return true;
        } else {
            return false;
        }
	}
	
	// prueft Schaltjahr eines Jahres
	private boolean checkSchaltjahr(int jahr) {
		if (jahr % 400 == 0) {
            return true;
        } else if (jahr % 100 == 0) {
            return false;
        } else if (jahr % 4 == 0) {
            return true;
        } else {
            return false;
        }
	}

	
}
