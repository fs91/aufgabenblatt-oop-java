package Abschreibungen;

class Abschreibung {

    private float preisAnschaffung;
    private int jahre;
    private float satzAbschreibung;

    // Konstruktor lineare Abschreibung
    Abschreibung(float preis, int jahre) {
        this.preisAnschaffung = preis;
        this.jahre = jahre;
    }

    // Konstruktor degressive Abschreibung
    Abschreibung(float preis, int jahre, float satz) {
        this.preisAnschaffung = preis;
        this.jahre = jahre;
        this.satzAbschreibung = satz;
    }

    public void schreibeLinear() {
        float ap = this.preisAnschaffung; // Anschaffungspreis
        int j = this.jahre; // Abschreibungsjahre
        float am = ap / j; // Abschreibungsmenge

        
        System.out.println("|  Jahre  |  Lineare Abschreibung");
        System.out.println("|    0    |      " + ap);
        for (int i = 0; i < j; i++) {
            ap -= am;
            System.out.println("|    " + (i+1) + "    |      " + ap);
        }
    }

    public void schreibeDegressiv() {
    	float ap = this.preisAnschaffung; // Anschaffungspreis
        int j = this.jahre; // Abschreibungsjahre
        float as = this.satzAbschreibung; // Abschreibungssatz
        float am = 1; // Abschreibungsmenge

        
        System.out.println("|  Jahre  |  Degressive Abschreibung");
        System.out.println("|    0    |      " + ap);
        for (int i = 0; i < j; i++) {
            am = ap * as / 100;
        	ap -= am;
            System.out.println("|    " + (i+1) + "    |      " + ap);
        }
    }

}