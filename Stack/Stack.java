package Stack;

class Stack {
	
	private int[] stack;
	
	// Konstruktor mit "Standardgroesse"
	Stack() {
		stack = new int[6];
	}
	
	// Konstruktor mit individueller Groesse
	Stack(int size) {
		stack = new int[size];
	}
	
	// legt Wert in den Stack
	public void push(int e) {
		int i = getLast();		// letzten freien Slot im Array
		if (i == stack.length) {
			extendArray();		// array wird erweitert
			i = getLast();		// neuer letzter Slot
		}
		stack[i] = e;
	}
	
	// nimmt Wert vom letzten Slot im Stack 
	public int pop() {
		int i = getLast();		// holt letztes "volle" Feld
		if (i == stack.length) {	// wenn Array voll ist, wird i das letzte Feld
			i = stack.length - 1;
		}
		int e = stack[i];
		stack[i] = 0;			// leert das Feld
		return e;
	}
	
	/**
	 *  sucht das letzte belegte bzw. freie Feld
	 *  wenn kein Feld 0 ist, dann wird die Array-Laenge 
	 *  als Abbruchbedingung zurueckgegeben
	 * 
	 */
	private int getLast() {
		for (int i = 0; i < stack.length; i++) {
			if (stack[i] == 0) {
				return i;
			}
		}
		return stack.length;
	}
	
	// erstellt stack mit doppelter Groesse
	// dumped tmp und alten stack
	private void extendArray() {
		int size = stack.length * 2;
		int[] tmp = new int[size];
		for (int i = 0; i < stack.length; i++) {
			tmp[i] = stack[i];
		}
		stack = null;
		stack = tmp;
		tmp = null;
	}
	
	// gibt stack groesse zurueck
	public int getStackSize() {
		return stack.length;
	}
	
	// output des stacks (fuer Tests)
	public void outputStack() {
		for (int i = 0; i < stack.length; i++) {
			System.out.print("[Slot " + i +"]: " + stack[i] + " | ");
		}
		System.out.println();
	}
	
}
