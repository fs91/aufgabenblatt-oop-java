package Stack;

public class Test {

	public static void main(String[] args) {
		Stack s1 = new Stack();
		Stack s2 = new Stack(4);
		
		System.out.println(s1.getStackSize());
		
		for (int i = 0; i < s1.getStackSize()+1; i++) {
			s1.push(i);
		}
		
		for (int i = 0; i < s2.getStackSize()+1; i++) {
			s2.push(i);
		}
		System.out.println("Stack 1 erstellt\n-------");
		s1.outputStack();
		System.out.println("\nStack 2 erstellt\n-------");
		s2.outputStack();
		
		System.out.println("\n-------------------------");
		
		s1.pop();
		s2.pop();
		System.out.println("\nStack 1 pop()\n-------");
		s1.outputStack();
		System.out.println("\nStack 2 pop()\n-------");
		s2.outputStack();
		
		System.out.println("\n-------------------------");
		
		s1.push(3);
		s2.push(4);
		System.out.println("\nStack 1 push(3)\n-------");
		s1.outputStack();
		System.out.println("\nStack 2 push(4)\n-------");
		s2.outputStack();
		
		System.out.println("\n-------------------------");
		
		s1.push(5);
		s2.push(3);
		System.out.println("\nStack 1 extend by push(5)\n-------");
		s1.outputStack();
		System.out.println("\nStack 2 extend by push(3)\n-------");
		s2.outputStack();
	}

}
