package Bruch;

public class Test {

	public static void main(String[] args) {
		Bruch b1 = new Bruch(1, 2);
		Bruch b2 = new Bruch(1, 4);
		
		System.out.println("Addition\n-------");
		Bruch ergAdd = b1.addBruch(b2);
		ergAdd.kuerzeBruch();
		System.out.print("gek�rzt: " );
		ergAdd.showBruch();
		
		System.out.println("\nSubtraktion\n-------");
		Bruch ergSub = b1.subBruch(b2);
		ergSub.kuerzeBruch();
		System.out.print("gek�rzt: " );
		ergSub.showBruch();
		
		System.out.println("\nMultiplikation\n-------");
		Bruch ergMul = b1.mulBruch(b2);
		ergMul.kuerzeBruch();
		System.out.print("gek�rzt: " );
		ergMul.showBruch();
		
		System.out.println("\nDivision\n-------");
		Bruch ergDiv = b1.divBruch(b2);
		ergDiv.kuerzeBruch();
		System.out.print("gek�rzt: " );
		ergDiv.showBruch();

	}

}