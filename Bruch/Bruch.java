package Bruch;

class Bruch {

	private int zaehler;
	private int nenner;
	
	Bruch(int zaehler, int nenner) {
		this.zaehler = zaehler;
		this.nenner = nenner;
	}
	
	public Bruch addBruch(Bruch b) {
		System.out.print(this.zaehler + "/" + this.nenner + " + " + 
				b.zaehler + "/" + b.nenner + " = ");
		Bruch ergB = new Bruch(
				(this.zaehler * b.nenner) + (b.zaehler * this.nenner),
				(this.nenner * b.nenner));
		ergB.showBruch();
		return ergB;
	}
	
	public Bruch subBruch(Bruch b) {
		System.out.print(this.zaehler + "/" + this.nenner + " - " + 
				b.zaehler + "/" + b.nenner + " = ");
		Bruch ergB = new Bruch(
				(this.zaehler * b.nenner) - (b.zaehler * this.nenner),
				(this.nenner * b.nenner));
		ergB.showBruch();
		return ergB;
	}
	
	public Bruch mulBruch(Bruch b) {
		System.out.print(this.zaehler + "/" + this.nenner + " * " + 
				b.zaehler + "/" + b.nenner + " = ");
		Bruch ergB = new Bruch(
				(this.zaehler * b.zaehler), 
				(this.nenner * b.nenner));
		ergB.showBruch();
		return ergB;
	}
	
	public Bruch divBruch(Bruch b) {
		System.out.print(this.zaehler + "/" + this.nenner + " / " + 
				b.zaehler + "/" + b.nenner + " = ");
		Bruch ergB = new Bruch(
				(this.zaehler * b.nenner),
				(this.nenner * b.zaehler));
		ergB.showBruch();
		return ergB;
	}
	
	private int ggt(int a, int b) {
		while (b > 0) {
			if (b > a) {
				int h = b;
				b = a;
				a = h;
			}
			a = a - b;
		}
		return a;
	}
	
	public Bruch kuerzeBruch() {
		int f = ggt(this.zaehler, this.nenner);
		this.zaehler = this.zaehler / f;
		this.nenner = this.nenner / f;
		return this;
	}
	
	public void showBruch() {
		System.out.print(this.zaehler + "/" + this.nenner + "\n");
	}

	
}
