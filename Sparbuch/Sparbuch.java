package Sparbuch;

public class Sparbuch {
	
	private int kontonummer;
	private double kapital;
	private double zinssatz;
	
	Sparbuch(int kn, int kap, int zs) {
		this.kontonummer = kn;
		this.kapital = kap;
		this.zinssatz = zs;
	}
	
	public void zahleEin(double betrag) {
		this.kapital = getKapital() + betrag;
	}
	
	public void hebeAb(double betrag) {
		this.kapital = getKapital() - betrag;
	}
	
	public double getErtrag(int jahr) {
		double kap = getKapital();
		for (int i = 0; i < jahr; i++) {
			kap += ((kap * getZinssatz()) / 100);
		}
		return (double) Math.round(kap * 100.0) / 100.0;
	}
	
	public void verzinse() {
		double zinsen = (getKapital() * getZinssatz()) / 100;
		this.kapital = getKapital() + zinsen;
	}
	
	public int getKontonummer() {
		return kontonummer;
	}

	public double getKapital() {
		return kapital;
	}

	public double getZinssatz() {
		return zinssatz;
	}

	
}
